package com.bver.converter.repo.bver;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bver.converter.model.bver.EventsCategories;

@Repository
public interface EventsCategoriesRepo extends JpaRepository<EventsCategories, Long> {

	@Override
    public List<EventsCategories> findAll();

}
