// Generated with g9.

package com.bver.converter.model.iso27001an;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Version;

@Entity(name="a14_bcm")
public class A14Bcm implements Serializable {

    /** Primary key. */
    protected static final String PK = "ref";

    /**
     * The optimistic lock. Available via standard bean get/set operations.
     */
    @Version
    @Column(name="LOCK_FLAG")
    private Integer lockFlag;

    /**
     * Access method for the lockFlag property.
     *
     * @return the current value of the lockFlag property
     */
    public Integer getLockFlag() {
        return lockFlag;
    }

    /**
     * Sets the value of the lockFlag property.
     *
     * @param aLockFlag the new value of the lockFlag property
     */
    public void setLockFlag(Integer aLockFlag) {
        lockFlag = aLockFlag;
    }

    @Id
    @Column(name="Ref", unique=true, nullable=false, length=128)
    private String ref;
    @Column(name="Security_controls", length=512)
    private String securityControls;
    @Column(name="Selected", length=10)
    private String selected;
    @Column(name="Excluded", length=10)
    private String excluded;
    @Column(name="Implemented", length=10)
    private String implemented;
    @Column(name="Justification", length=1024)
    private String justification;
    @Column(name="SIDSS_Ref")
    private String sidssRef;

    /** Default constructor. */
    public A14Bcm() {
        super();
    }

    /**
     * Access method for ref.
     *
     * @return the current value of ref
     */
    public String getRef() {
        return ref;
    }

    /**
     * Setter method for ref.
     *
     * @param aRef the new value for ref
     */
    public void setRef(String aRef) {
        ref = aRef;
    }

    /**
     * Access method for securityControls.
     *
     * @return the current value of securityControls
     */
    public String getSecurityControls() {
        return securityControls;
    }

    /**
     * Setter method for securityControls.
     *
     * @param aSecurityControls the new value for securityControls
     */
    public void setSecurityControls(String aSecurityControls) {
        securityControls = aSecurityControls;
    }

    /**
     * Access method for selected.
     *
     * @return the current value of selected
     */
    public String getSelected() {
        return selected;
    }

    /**
     * Setter method for selected.
     *
     * @param aSelected the new value for selected
     */
    public void setSelected(String aSelected) {
        selected = aSelected;
    }

    /**
     * Access method for excluded.
     *
     * @return the current value of excluded
     */
    public String getExcluded() {
        return excluded;
    }

    /**
     * Setter method for excluded.
     *
     * @param aExcluded the new value for excluded
     */
    public void setExcluded(String aExcluded) {
        excluded = aExcluded;
    }

    /**
     * Access method for implemented.
     *
     * @return the current value of implemented
     */
    public String getImplemented() {
        return implemented;
    }

    /**
     * Setter method for implemented.
     *
     * @param aImplemented the new value for implemented
     */
    public void setImplemented(String aImplemented) {
        implemented = aImplemented;
    }

    /**
     * Access method for justification.
     *
     * @return the current value of justification
     */
    public String getJustification() {
        return justification;
    }

    /**
     * Setter method for justification.
     *
     * @param aJustification the new value for justification
     */
    public void setJustification(String aJustification) {
        justification = aJustification;
    }

    /**
     * Access method for sidssRef.
     *
     * @return the current value of sidssRef
     */
    public String getSidssRef() {
        return sidssRef;
    }

    /**
     * Setter method for sidssRef.
     *
     * @param aSidssRef the new value for sidssRef
     */
    public void setSidssRef(String aSidssRef) {
        sidssRef = aSidssRef;
    }

    /**
     * Compares the key for this instance with another A14Bcm.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class A14Bcm and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof A14Bcm)) {
            return false;
        }
        A14Bcm that = (A14Bcm) other;
        Object myRef = this.getRef();
        Object yourRef = that.getRef();
        if (myRef==null ? yourRef!=null : !myRef.equals(yourRef)) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another A14Bcm.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof A14Bcm)) return false;
        return this.equalKeys(other) && ((A14Bcm)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        if (getRef() == null) {
            i = 0;
        } else {
            i = getRef().hashCode();
        }
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[A14Bcm |");
        sb.append(" ref=").append(getRef());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("ref", getRef());
        return ret;
    }

}
