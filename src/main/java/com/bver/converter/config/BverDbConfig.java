package com.bver.converter.config;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
  entityManagerFactoryRef = "bverEntityManagerFactory",
  transactionManagerRef = "bverTransactionManager",
  basePackages = { "com.bver.converter.repo.bver" }
)
public class BverDbConfig {
	private static Logger log = LoggerFactory.getLogger(BverDbConfig.class);
	
	@Bean(name = "bverDataSource")
	@ConfigurationProperties(prefix = "spring.datasource")
	public DataSource dataSource() {
		log.info("Create Datasource");
		return DataSourceBuilder.create().build();
	}

	@Bean(name = "bverEntityManagerFactory")
	public LocalContainerEntityManagerFactoryBean bverEntityManagerFactory(EntityManagerFactoryBuilder builder,
			@Qualifier("bverDataSource") DataSource dataSource) {
		return builder.dataSource(dataSource).packages("com.bver.converter.model.bver").persistenceUnit("bver").build();
	}

	@Bean(name = "bverTransactionManager")
	public PlatformTransactionManager bverTransactionManager(
			@Qualifier("bverEntityManagerFactory") EntityManagerFactory bverEntityManagerFactory) {
		return new JpaTransactionManager(bverEntityManagerFactory);
	}
}
