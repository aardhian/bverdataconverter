// Generated with g9.

package com.bver.converter.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Version;

@Entity(name="config")
public class Config implements Serializable {

    /** Primary key. */
    protected static final String PK = "conf";

    /**
     * The optimistic lock. Available via standard bean get/set operations.
     */
    @Version
    @Column(name="LOCK_FLAG")
    private Integer lockFlag;

    /**
     * Access method for the lockFlag property.
     *
     * @return the current value of the lockFlag property
     */
    public Integer getLockFlag() {
        return lockFlag;
    }

    /**
     * Sets the value of the lockFlag property.
     *
     * @param aLockFlag the new value of the lockFlag property
     */
    public void setLockFlag(Integer aLockFlag) {
        lockFlag = aLockFlag;
    }

    @Id
    @Column(unique=true, nullable=false, length=255)
    private String conf;
    private String value;

    /** Default constructor. */
    public Config() {
        super();
    }

    /**
     * Access method for conf.
     *
     * @return the current value of conf
     */
    public String getConf() {
        return conf;
    }

    /**
     * Setter method for conf.
     *
     * @param aConf the new value for conf
     */
    public void setConf(String aConf) {
        conf = aConf;
    }

    /**
     * Access method for value.
     *
     * @return the current value of value
     */
    public String getValue() {
        return value;
    }

    /**
     * Setter method for value.
     *
     * @param aValue the new value for value
     */
    public void setValue(String aValue) {
        value = aValue;
    }

    /**
     * Compares the key for this instance with another Config.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Config and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Config)) {
            return false;
        }
        Config that = (Config) other;
        Object myConf = this.getConf();
        Object yourConf = that.getConf();
        if (myConf==null ? yourConf!=null : !myConf.equals(yourConf)) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Config.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Config)) return false;
        return this.equalKeys(other) && ((Config)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        if (getConf() == null) {
            i = 0;
        } else {
            i = getConf().hashCode();
        }
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Config |");
        sb.append(" conf=").append(getConf());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("conf", getConf());
        return ret;
    }

}
