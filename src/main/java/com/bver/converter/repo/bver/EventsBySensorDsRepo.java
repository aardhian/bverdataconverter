package com.bver.converter.repo.bver;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bver.converter.model.bver.EventsBySensorDs;

@Repository
public interface EventsBySensorDsRepo extends JpaRepository<EventsBySensorDs, Long> {

	@Override
    public List<EventsBySensorDs> findAll();
	
}
