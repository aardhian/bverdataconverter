package com.bver.converter.repo.bver;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bver.converter.model.bver.EventsSiemVsLogger;

@Repository
public interface EventsSiemVsLoggerRepo extends JpaRepository<EventsSiemVsLogger, Long> {

	@Override
    public List<EventsSiemVsLogger> findAll();

}
