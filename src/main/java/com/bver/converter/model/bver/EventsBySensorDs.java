// Generated with g9.

package com.bver.converter.model.bver;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

@Entity(name="EVENTS_BY_SENSOR_DS")
public class EventsBySensorDs implements Serializable {

    /** Primary key. */
    protected static final String PK = "id";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="ID", unique=true, nullable=false, precision=19)
    private long id;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="CREATED_DATE", nullable=false)
    private Date createdDate;
    @Column(name="DEVICE_ID", length=255)
    private String deviceId;
    @Column(name="PLUGIN_ID", length=255)
    private String pluginId;
    @Column(name="NAME", length=255)
    private String name;
    @Column(name="EVENT_CNT", nullable=false, precision=19)
    private long eventCnt;

    /** Default constructor. */
    public EventsBySensorDs() {
        super();
    }

    /**
     * Access method for id.
     *
     * @return the current value of id
     */
    public long getId() {
        return id;
    }

    /**
     * Setter method for id.
     *
     * @param aId the new value for id
     */
    public void setId(long aId) {
        id = aId;
    }

    /**
     * Access method for createdDate.
     *
     * @return the current value of createdDate
     */
    public Date getCreatedDate() {
        return createdDate;
    }

    /**
     * Setter method for createdDate.
     *
     * @param aCreatedDate the new value for createdDate
     */
    public void setCreatedDate(Date aCreatedDate) {
        createdDate = aCreatedDate;
    }

    /**
     * Access method for deviceId.
     *
     * @return the current value of deviceId
     */
    public String getDeviceId() {
        return deviceId;
    }

    /**
     * Setter method for deviceId.
     *
     * @param aDeviceId the new value for deviceId
     */
    public void setDeviceId(String aDeviceId) {
        deviceId = aDeviceId;
    }

    /**
     * Access method for pluginId.
     *
     * @return the current value of pluginId
     */
    public String getPluginId() {
        return pluginId;
    }

    /**
     * Setter method for pluginId.
     *
     * @param aPluginId the new value for pluginId
     */
    public void setPluginId(String aPluginId) {
        pluginId = aPluginId;
    }

    /**
     * Access method for name.
     *
     * @return the current value of name
     */
    public String getName() {
        return name;
    }

    /**
     * Setter method for name.
     *
     * @param aName the new value for name
     */
    public void setName(String aName) {
        name = aName;
    }

    /**
     * Access method for eventCnt.
     *
     * @return the current value of eventCnt
     */
    public long getEventCnt() {
        return eventCnt;
    }

    /**
     * Setter method for eventCnt.
     *
     * @param aEventCnt the new value for eventCnt
     */
    public void setEventCnt(long aEventCnt) {
        eventCnt = aEventCnt;
    }

    /**
     * Compares the key for this instance with another EventsBySensorDs.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class EventsBySensorDs and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof EventsBySensorDs)) {
            return false;
        }
        EventsBySensorDs that = (EventsBySensorDs) other;
        if (this.getId() != that.getId()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another EventsBySensorDs.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof EventsBySensorDs)) return false;
        return this.equalKeys(other) && ((EventsBySensorDs)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = (int)(getId() ^ (getId()>>>32));
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[EventsBySensorDs |");
        sb.append(" id=").append(getId());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("id", Long.valueOf(getId()));
        return ret;
    }

}
