// Generated with g9.

package com.bver.converter.model.asec;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Version;

@Entity(name="notification")
public class Notification implements Serializable {

    /** Primary key. */
    protected static final String PK = "id";

    /**
     * The optimistic lock. Available via standard bean get/set operations.
     */
    @Version
    @Column(name="LOCK_FLAG")
    private Integer lockFlag;

    /**
     * Access method for the lockFlag property.
     *
     * @return the current value of the lockFlag property
     */
    public Integer getLockFlag() {
        return lockFlag;
    }

    /**
     * Sets the value of the lockFlag property.
     *
     * @param aLockFlag the new value of the lockFlag property
     */
    public void setLockFlag(Integer aLockFlag) {
        lockFlag = aLockFlag;
    }

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(unique=true, nullable=false, precision=19)
    private long id;
    @Column(name="plugin_id", nullable=false, precision=10)
    private int pluginId;
    @Column(name="rule_name", nullable=false, length=45)
    private String ruleName;
    @Column(name="log_file", nullable=false, length=45)
    private String logFile;
    @Column(precision=3)
    private short ignore;
    @Column(name="ignore_timestamp")
    private LocalDateTime ignoreTimestamp;
    @Column(name="sensor_id", nullable=false, length=16)
    private byte[] sensorId;
    @Column(nullable=false)
    private LocalDateTime datetime;

    /** Default constructor. */
    public Notification() {
        super();
    }

    /**
     * Access method for id.
     *
     * @return the current value of id
     */
    public long getId() {
        return id;
    }

    /**
     * Setter method for id.
     *
     * @param aId the new value for id
     */
    public void setId(long aId) {
        id = aId;
    }

    /**
     * Access method for pluginId.
     *
     * @return the current value of pluginId
     */
    public int getPluginId() {
        return pluginId;
    }

    /**
     * Setter method for pluginId.
     *
     * @param aPluginId the new value for pluginId
     */
    public void setPluginId(int aPluginId) {
        pluginId = aPluginId;
    }

    /**
     * Access method for ruleName.
     *
     * @return the current value of ruleName
     */
    public String getRuleName() {
        return ruleName;
    }

    /**
     * Setter method for ruleName.
     *
     * @param aRuleName the new value for ruleName
     */
    public void setRuleName(String aRuleName) {
        ruleName = aRuleName;
    }

    /**
     * Access method for logFile.
     *
     * @return the current value of logFile
     */
    public String getLogFile() {
        return logFile;
    }

    /**
     * Setter method for logFile.
     *
     * @param aLogFile the new value for logFile
     */
    public void setLogFile(String aLogFile) {
        logFile = aLogFile;
    }

    /**
     * Access method for ignore.
     *
     * @return the current value of ignore
     */
    public short getIgnore() {
        return ignore;
    }

    /**
     * Setter method for ignore.
     *
     * @param aIgnore the new value for ignore
     */
    public void setIgnore(short aIgnore) {
        ignore = aIgnore;
    }

    /**
     * Access method for ignoreTimestamp.
     *
     * @return the current value of ignoreTimestamp
     */
    public LocalDateTime getIgnoreTimestamp() {
        return ignoreTimestamp;
    }

    /**
     * Setter method for ignoreTimestamp.
     *
     * @param aIgnoreTimestamp the new value for ignoreTimestamp
     */
    public void setIgnoreTimestamp(LocalDateTime aIgnoreTimestamp) {
        ignoreTimestamp = aIgnoreTimestamp;
    }

    /**
     * Access method for sensorId.
     *
     * @return the current value of sensorId
     */
    public byte[] getSensorId() {
        return sensorId;
    }

    /**
     * Setter method for sensorId.
     *
     * @param aSensorId the new value for sensorId
     */
    public void setSensorId(byte[] aSensorId) {
        sensorId = aSensorId;
    }

    /**
     * Access method for datetime.
     *
     * @return the current value of datetime
     */
    public LocalDateTime getDatetime() {
        return datetime;
    }

    /**
     * Setter method for datetime.
     *
     * @param aDatetime the new value for datetime
     */
    public void setDatetime(LocalDateTime aDatetime) {
        datetime = aDatetime;
    }

    /**
     * Compares the key for this instance with another Notification.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Notification and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Notification)) {
            return false;
        }
        Notification that = (Notification) other;
        if (this.getId() != that.getId()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Notification.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Notification)) return false;
        return this.equalKeys(other) && ((Notification)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = (int)(getId() ^ (getId()>>>32));
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Notification |");
        sb.append(" id=").append(getId());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("id", Long.valueOf(getId()));
        return ret;
    }

}
