// Generated with g9.

package com.bver.converter.model.api;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Version;

@Entity(name="user_perms")
@IdClass(UserPerms.UserPermsId.class)
public class UserPerms implements Serializable {

    /**
     * IdClass for primary key when using JPA annotations
     */
    public class UserPermsId implements Serializable {
        java.lang.String login;
        byte[] componentId;
    }

    /** Primary key. */
    protected static final String PK = "UserPermsPrimary";

    /**
     * The optimistic lock. Available via standard bean get/set operations.
     */
    @Version
    @Column(name="LOCK_FLAG")
    private Integer lockFlag;

    /**
     * Access method for the lockFlag property.
     *
     * @return the current value of the lockFlag property
     */
    public Integer getLockFlag() {
        return lockFlag;
    }

    /**
     * Sets the value of the lockFlag property.
     *
     * @param aLockFlag the new value of the lockFlag property
     */
    public void setLockFlag(Integer aLockFlag) {
        lockFlag = aLockFlag;
    }

    @Id
    @Column(nullable=false, length=64)
    private String login;
    @Id
    @Column(name="component_id", nullable=false, length=16)
    private byte[] componentId;

    /** Default constructor. */
    public UserPerms() {
        super();
    }

    /**
     * Access method for login.
     *
     * @return the current value of login
     */
    public String getLogin() {
        return login;
    }

    /**
     * Setter method for login.
     *
     * @param aLogin the new value for login
     */
    public void setLogin(String aLogin) {
        login = aLogin;
    }

    /**
     * Access method for componentId.
     *
     * @return the current value of componentId
     */
    public byte[] getComponentId() {
        return componentId;
    }

    /**
     * Setter method for componentId.
     *
     * @param aComponentId the new value for componentId
     */
    public void setComponentId(byte[] aComponentId) {
        componentId = aComponentId;
    }

    /**
     * Compares the key for this instance with another UserPerms.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class UserPerms and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof UserPerms)) {
            return false;
        }
        UserPerms that = (UserPerms) other;
        Object myLogin = this.getLogin();
        Object yourLogin = that.getLogin();
        if (myLogin==null ? yourLogin!=null : !myLogin.equals(yourLogin)) {
            return false;
        }
        Object myComponentId = this.getComponentId();
        Object yourComponentId = that.getComponentId();
        if (myComponentId==null ? yourComponentId!=null : !myComponentId.equals(yourComponentId)) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another UserPerms.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof UserPerms)) return false;
        return this.equalKeys(other) && ((UserPerms)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        if (getLogin() == null) {
            i = 0;
        } else {
            i = getLogin().hashCode();
        }
        result = 37*result + i;
        if (getComponentId() == null) {
            i = 0;
        } else {
            i = getComponentId().hashCode();
        }
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[UserPerms |");
        sb.append(" login=").append(getLogin());
        sb.append(" componentId=").append(getComponentId());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("login", getLogin());
        ret.put("componentId", getComponentId());
        return ret;
    }

}
