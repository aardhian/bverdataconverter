package com.bver.converter.beans;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextClosedEvent;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import com.bver.converter.service.BverDataConverterService;

@Component
public class LongRunningProcessBean {

    private static final Logger LOG = LoggerFactory.getLogger(LongRunningProcessBean.class);

    @Autowired
    private BverDataConverterService bverDataConverterService;

    @PostConstruct
    public void runTaskOnStartup() {
        LOG.info("runTaskOnStartup entering");
        	try {
        		
        		LOG.info("Start executeEventsBySensorDs");
        		bverDataConverterService.executeEventsBySensorDs();
        		
        		LOG.info("Start executeEventsCategories");
        		bverDataConverterService.executeEventsCategories();
        		
        		LOG.info("Start executeEventsSecTrenLastDay");
        		bverDataConverterService.executeEventsSecTrenLastDay();
        		
        		LOG.info("Start executeEventsSecTrenLastWeek");
        		bverDataConverterService.executeEventsSecTrenLastWeek();
        		
        		LOG.info("Start executeEventsSiemVsLogger");
        		bverDataConverterService.executeEventsSiemVsLogger();
        		
        	} catch (Exception e) {
                LOG.error("Error while executing task", e);
            } finally {
            	LOG.info("Finish executeEventsBySensorDs");
            }
        
        LOG.info("runTaskOnStartup exiting");
    }
}
