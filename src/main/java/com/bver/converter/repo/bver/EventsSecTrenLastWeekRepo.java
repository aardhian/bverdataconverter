package com.bver.converter.repo.bver;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bver.converter.model.bver.EventsSecTrenLastWeek;

@Repository
public interface EventsSecTrenLastWeekRepo extends JpaRepository<EventsSecTrenLastWeek, Long> {

	@Override
    public List<EventsSecTrenLastWeek> findAll();

}
