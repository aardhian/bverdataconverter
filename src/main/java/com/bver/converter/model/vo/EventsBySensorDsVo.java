package com.bver.converter.model.vo;

import java.util.Date;

public class EventsBySensorDsVo {
    private long id;
    private Date createdDate;
    private String deviceId;
    private String pluginId;
    private String name;
    private long eventCnt;
    
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public String getDeviceId() {
		return deviceId;
	}
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
	public String getPluginId() {
		return pluginId;
	}
	public void setPluginId(String pluginId) {
		this.pluginId = pluginId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public long getEventCnt() {
		return eventCnt;
	}
	public void setEventCnt(long eventCnt) {
		this.eventCnt = eventCnt;
	}
    
    
}
