// Generated with g9.

package com.bver.converter.model;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Version;

@Entity(name="acid_event")
public class AcidEvent implements Serializable {

    /**
     * The optimistic lock. Available via standard bean get/set operations.
     */
    @Version
    @Column(name="LOCK_FLAG")
    private Integer lockFlag;

    /**
     * Access method for the lockFlag property.
     *
     * @return the current value of the lockFlag property
     */
    public Integer getLockFlag() {
        return lockFlag;
    }

    /**
     * Sets the value of the lockFlag property.
     *
     * @param aLockFlag the new value of the lockFlag property
     */
    public void setLockFlag(Integer aLockFlag) {
        lockFlag = aLockFlag;
    }

    @Id
    @Column(nullable=false, length=16)
    private byte[] id;
    @Column(name="device_id", nullable=false, precision=10)
    private int deviceId;
    @Column(nullable=false, length=16)
    private byte[] ctx;
    @Column(nullable=false)
    private LocalDateTime timestamp;
    @Column(name="ip_src", length=16)
    private byte[] ipSrc;
    @Column(name="ip_dst", length=16)
    private byte[] ipDst;
    @Column(name="ip_proto", precision=10)
    private int ipProto;
    @Column(name="layer4_sport", precision=5)
    private short layer4Sport;
    @Column(name="layer4_dport", precision=5)
    private short layer4Dport;
    @Column(name="ossim_priority", precision=3)
    private short ossimPriority;
    @Column(name="ossim_reliability", precision=3)
    private short ossimReliability;
    @Column(name="ossim_asset_src", precision=3)
    private short ossimAssetSrc;
    @Column(name="ossim_asset_dst", precision=3)
    private short ossimAssetDst;
    @Column(name="ossim_risk_c", precision=3)
    private short ossimRiskC;
    @Column(name="ossim_risk_a", precision=3)
    private short ossimRiskA;
    @Column(name="plugin_id", precision=10)
    private int pluginId;
    @Column(name="plugin_sid", precision=10)
    private int pluginSid;
    @Column(nullable=false, precision=12)
    private float tzone;
    @Column(name="ossim_correlation", precision=3)
    private short ossimCorrelation;
    @Column(name="src_hostname", length=64)
    private String srcHostname;
    @Column(name="dst_hostname", length=64)
    private String dstHostname;
    @Column(name="src_mac", length=6)
    private byte[] srcMac;
    @Column(name="dst_mac", length=6)
    private byte[] dstMac;
    @Column(name="src_host", length=16)
    private byte[] srcHost;
    @Column(name="dst_host", length=16)
    private byte[] dstHost;
    @Column(name="src_net", length=16)
    private byte[] srcNet;
    @Column(name="dst_net", length=16)
    private byte[] dstNet;

    /** Default constructor. */
    public AcidEvent() {
        super();
    }

    /**
     * Access method for id.
     *
     * @return the current value of id
     */
    public byte[] getId() {
        return id;
    }

    /**
     * Setter method for id.
     *
     * @param aId the new value for id
     */
    public void setId(byte[] aId) {
        id = aId;
    }

    /**
     * Access method for deviceId.
     *
     * @return the current value of deviceId
     */
    public int getDeviceId() {
        return deviceId;
    }

    /**
     * Setter method for deviceId.
     *
     * @param aDeviceId the new value for deviceId
     */
    public void setDeviceId(int aDeviceId) {
        deviceId = aDeviceId;
    }

    /**
     * Access method for ctx.
     *
     * @return the current value of ctx
     */
    public byte[] getCtx() {
        return ctx;
    }

    /**
     * Setter method for ctx.
     *
     * @param aCtx the new value for ctx
     */
    public void setCtx(byte[] aCtx) {
        ctx = aCtx;
    }

    /**
     * Access method for timestamp.
     *
     * @return the current value of timestamp
     */
    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    /**
     * Setter method for timestamp.
     *
     * @param aTimestamp the new value for timestamp
     */
    public void setTimestamp(LocalDateTime aTimestamp) {
        timestamp = aTimestamp;
    }

    /**
     * Access method for ipSrc.
     *
     * @return the current value of ipSrc
     */
    public byte[] getIpSrc() {
        return ipSrc;
    }

    /**
     * Setter method for ipSrc.
     *
     * @param aIpSrc the new value for ipSrc
     */
    public void setIpSrc(byte[] aIpSrc) {
        ipSrc = aIpSrc;
    }

    /**
     * Access method for ipDst.
     *
     * @return the current value of ipDst
     */
    public byte[] getIpDst() {
        return ipDst;
    }

    /**
     * Setter method for ipDst.
     *
     * @param aIpDst the new value for ipDst
     */
    public void setIpDst(byte[] aIpDst) {
        ipDst = aIpDst;
    }

    /**
     * Access method for ipProto.
     *
     * @return the current value of ipProto
     */
    public int getIpProto() {
        return ipProto;
    }

    /**
     * Setter method for ipProto.
     *
     * @param aIpProto the new value for ipProto
     */
    public void setIpProto(int aIpProto) {
        ipProto = aIpProto;
    }

    /**
     * Access method for layer4Sport.
     *
     * @return the current value of layer4Sport
     */
    public short getLayer4Sport() {
        return layer4Sport;
    }

    /**
     * Setter method for layer4Sport.
     *
     * @param aLayer4Sport the new value for layer4Sport
     */
    public void setLayer4Sport(short aLayer4Sport) {
        layer4Sport = aLayer4Sport;
    }

    /**
     * Access method for layer4Dport.
     *
     * @return the current value of layer4Dport
     */
    public short getLayer4Dport() {
        return layer4Dport;
    }

    /**
     * Setter method for layer4Dport.
     *
     * @param aLayer4Dport the new value for layer4Dport
     */
    public void setLayer4Dport(short aLayer4Dport) {
        layer4Dport = aLayer4Dport;
    }

    /**
     * Access method for ossimPriority.
     *
     * @return the current value of ossimPriority
     */
    public short getOssimPriority() {
        return ossimPriority;
    }

    /**
     * Setter method for ossimPriority.
     *
     * @param aOssimPriority the new value for ossimPriority
     */
    public void setOssimPriority(short aOssimPriority) {
        ossimPriority = aOssimPriority;
    }

    /**
     * Access method for ossimReliability.
     *
     * @return the current value of ossimReliability
     */
    public short getOssimReliability() {
        return ossimReliability;
    }

    /**
     * Setter method for ossimReliability.
     *
     * @param aOssimReliability the new value for ossimReliability
     */
    public void setOssimReliability(short aOssimReliability) {
        ossimReliability = aOssimReliability;
    }

    /**
     * Access method for ossimAssetSrc.
     *
     * @return the current value of ossimAssetSrc
     */
    public short getOssimAssetSrc() {
        return ossimAssetSrc;
    }

    /**
     * Setter method for ossimAssetSrc.
     *
     * @param aOssimAssetSrc the new value for ossimAssetSrc
     */
    public void setOssimAssetSrc(short aOssimAssetSrc) {
        ossimAssetSrc = aOssimAssetSrc;
    }

    /**
     * Access method for ossimAssetDst.
     *
     * @return the current value of ossimAssetDst
     */
    public short getOssimAssetDst() {
        return ossimAssetDst;
    }

    /**
     * Setter method for ossimAssetDst.
     *
     * @param aOssimAssetDst the new value for ossimAssetDst
     */
    public void setOssimAssetDst(short aOssimAssetDst) {
        ossimAssetDst = aOssimAssetDst;
    }

    /**
     * Access method for ossimRiskC.
     *
     * @return the current value of ossimRiskC
     */
    public short getOssimRiskC() {
        return ossimRiskC;
    }

    /**
     * Setter method for ossimRiskC.
     *
     * @param aOssimRiskC the new value for ossimRiskC
     */
    public void setOssimRiskC(short aOssimRiskC) {
        ossimRiskC = aOssimRiskC;
    }

    /**
     * Access method for ossimRiskA.
     *
     * @return the current value of ossimRiskA
     */
    public short getOssimRiskA() {
        return ossimRiskA;
    }

    /**
     * Setter method for ossimRiskA.
     *
     * @param aOssimRiskA the new value for ossimRiskA
     */
    public void setOssimRiskA(short aOssimRiskA) {
        ossimRiskA = aOssimRiskA;
    }

    /**
     * Access method for pluginId.
     *
     * @return the current value of pluginId
     */
    public int getPluginId() {
        return pluginId;
    }

    /**
     * Setter method for pluginId.
     *
     * @param aPluginId the new value for pluginId
     */
    public void setPluginId(int aPluginId) {
        pluginId = aPluginId;
    }

    /**
     * Access method for pluginSid.
     *
     * @return the current value of pluginSid
     */
    public int getPluginSid() {
        return pluginSid;
    }

    /**
     * Setter method for pluginSid.
     *
     * @param aPluginSid the new value for pluginSid
     */
    public void setPluginSid(int aPluginSid) {
        pluginSid = aPluginSid;
    }

    /**
     * Access method for tzone.
     *
     * @return the current value of tzone
     */
    public float getTzone() {
        return tzone;
    }

    /**
     * Setter method for tzone.
     *
     * @param aTzone the new value for tzone
     */
    public void setTzone(float aTzone) {
        tzone = aTzone;
    }

    /**
     * Access method for ossimCorrelation.
     *
     * @return the current value of ossimCorrelation
     */
    public short getOssimCorrelation() {
        return ossimCorrelation;
    }

    /**
     * Setter method for ossimCorrelation.
     *
     * @param aOssimCorrelation the new value for ossimCorrelation
     */
    public void setOssimCorrelation(short aOssimCorrelation) {
        ossimCorrelation = aOssimCorrelation;
    }

    /**
     * Access method for srcHostname.
     *
     * @return the current value of srcHostname
     */
    public String getSrcHostname() {
        return srcHostname;
    }

    /**
     * Setter method for srcHostname.
     *
     * @param aSrcHostname the new value for srcHostname
     */
    public void setSrcHostname(String aSrcHostname) {
        srcHostname = aSrcHostname;
    }

    /**
     * Access method for dstHostname.
     *
     * @return the current value of dstHostname
     */
    public String getDstHostname() {
        return dstHostname;
    }

    /**
     * Setter method for dstHostname.
     *
     * @param aDstHostname the new value for dstHostname
     */
    public void setDstHostname(String aDstHostname) {
        dstHostname = aDstHostname;
    }

    /**
     * Access method for srcMac.
     *
     * @return the current value of srcMac
     */
    public byte[] getSrcMac() {
        return srcMac;
    }

    /**
     * Setter method for srcMac.
     *
     * @param aSrcMac the new value for srcMac
     */
    public void setSrcMac(byte[] aSrcMac) {
        srcMac = aSrcMac;
    }

    /**
     * Access method for dstMac.
     *
     * @return the current value of dstMac
     */
    public byte[] getDstMac() {
        return dstMac;
    }

    /**
     * Setter method for dstMac.
     *
     * @param aDstMac the new value for dstMac
     */
    public void setDstMac(byte[] aDstMac) {
        dstMac = aDstMac;
    }

    /**
     * Access method for srcHost.
     *
     * @return the current value of srcHost
     */
    public byte[] getSrcHost() {
        return srcHost;
    }

    /**
     * Setter method for srcHost.
     *
     * @param aSrcHost the new value for srcHost
     */
    public void setSrcHost(byte[] aSrcHost) {
        srcHost = aSrcHost;
    }

    /**
     * Access method for dstHost.
     *
     * @return the current value of dstHost
     */
    public byte[] getDstHost() {
        return dstHost;
    }

    /**
     * Setter method for dstHost.
     *
     * @param aDstHost the new value for dstHost
     */
    public void setDstHost(byte[] aDstHost) {
        dstHost = aDstHost;
    }

    /**
     * Access method for srcNet.
     *
     * @return the current value of srcNet
     */
    public byte[] getSrcNet() {
        return srcNet;
    }

    /**
     * Setter method for srcNet.
     *
     * @param aSrcNet the new value for srcNet
     */
    public void setSrcNet(byte[] aSrcNet) {
        srcNet = aSrcNet;
    }

    /**
     * Access method for dstNet.
     *
     * @return the current value of dstNet
     */
    public byte[] getDstNet() {
        return dstNet;
    }

    /**
     * Setter method for dstNet.
     *
     * @param aDstNet the new value for dstNet
     */
    public void setDstNet(byte[] aDstNet) {
        dstNet = aDstNet;
    }

}
