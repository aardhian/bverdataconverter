// Generated with g9.

package com.bver.converter.model.asec;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Version;

@Entity(name="suggestion_pattern")
public class SuggestionPattern implements Serializable {

    /** Primary key. */
    protected static final String PK = "id";

    /**
     * The optimistic lock. Available via standard bean get/set operations.
     */
    @Version
    @Column(name="LOCK_FLAG")
    private Integer lockFlag;

    /**
     * Access method for the lockFlag property.
     *
     * @return the current value of the lockFlag property
     */
    public Integer getLockFlag() {
        return lockFlag;
    }

    /**
     * Sets the value of the lockFlag property.
     *
     * @param aLockFlag the new value of the lockFlag property
     */
    public void setLockFlag(Integer aLockFlag) {
        lockFlag = aLockFlag;
    }

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(unique=true, nullable=false, precision=19)
    private long id;
    @Column(name="suggestion_group_id", nullable=false, length=16)
    private byte[] suggestionGroupId;
    @Column(name="pattern_json", nullable=false)
    private String patternJson;
    @Column(nullable=false, length=3)
    private boolean status;

    /** Default constructor. */
    public SuggestionPattern() {
        super();
    }

    /**
     * Access method for id.
     *
     * @return the current value of id
     */
    public long getId() {
        return id;
    }

    /**
     * Setter method for id.
     *
     * @param aId the new value for id
     */
    public void setId(long aId) {
        id = aId;
    }

    /**
     * Access method for suggestionGroupId.
     *
     * @return the current value of suggestionGroupId
     */
    public byte[] getSuggestionGroupId() {
        return suggestionGroupId;
    }

    /**
     * Setter method for suggestionGroupId.
     *
     * @param aSuggestionGroupId the new value for suggestionGroupId
     */
    public void setSuggestionGroupId(byte[] aSuggestionGroupId) {
        suggestionGroupId = aSuggestionGroupId;
    }

    /**
     * Access method for patternJson.
     *
     * @return the current value of patternJson
     */
    public String getPatternJson() {
        return patternJson;
    }

    /**
     * Setter method for patternJson.
     *
     * @param aPatternJson the new value for patternJson
     */
    public void setPatternJson(String aPatternJson) {
        patternJson = aPatternJson;
    }

    /**
     * Access method for status.
     *
     * @return true if and only if status is currently true
     */
    public boolean getStatus() {
        return status;
    }

    /**
     * Setter method for status.
     *
     * @param aStatus the new value for status
     */
    public void setStatus(boolean aStatus) {
        status = aStatus;
    }

    /**
     * Compares the key for this instance with another SuggestionPattern.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class SuggestionPattern and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof SuggestionPattern)) {
            return false;
        }
        SuggestionPattern that = (SuggestionPattern) other;
        if (this.getId() != that.getId()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another SuggestionPattern.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof SuggestionPattern)) return false;
        return this.equalKeys(other) && ((SuggestionPattern)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = (int)(getId() ^ (getId()>>>32));
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[SuggestionPattern |");
        sb.append(" id=").append(getId());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("id", Long.valueOf(getId()));
        return ret;
    }

}
