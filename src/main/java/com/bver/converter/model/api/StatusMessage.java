// Generated with g9.

package com.bver.converter.model.api;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Version;

@Entity(name="status_message")
public class StatusMessage implements Serializable {

    /** Primary key. */
    protected static final String PK = "id";

    /**
     * The optimistic lock. Available via standard bean get/set operations.
     */
    @Version
    @Column(name="LOCK_FLAG")
    private Integer lockFlag;

    /**
     * Access method for the lockFlag property.
     *
     * @return the current value of the lockFlag property
     */
    public Integer getLockFlag() {
        return lockFlag;
    }

    /**
     * Sets the value of the lockFlag property.
     *
     * @param aLockFlag the new value of the lockFlag property
     */
    public void setLockFlag(Integer aLockFlag) {
        lockFlag = aLockFlag;
    }

    @Id
    @Column(unique=true, nullable=false, length=16)
    private byte[] id;
    @Column(nullable=false, length=3)
    private boolean level;
    private String title;
    private String description;
    @Column(nullable=false, length=20)
    private String type;
    private LocalDateTime expire;
    private String actions;
    @Column(name="alternative_actions")
    private String alternativeActions;
    @Column(name="message_role")
    private String messageRole;
    @Column(name="action_role")
    private String actionRole;
    @Column(length=32)
    private String source;

    /** Default constructor. */
    public StatusMessage() {
        super();
    }

    /**
     * Access method for id.
     *
     * @return the current value of id
     */
    public byte[] getId() {
        return id;
    }

    /**
     * Setter method for id.
     *
     * @param aId the new value for id
     */
    public void setId(byte[] aId) {
        id = aId;
    }

    /**
     * Access method for level.
     *
     * @return true if and only if level is currently true
     */
    public boolean getLevel() {
        return level;
    }

    /**
     * Setter method for level.
     *
     * @param aLevel the new value for level
     */
    public void setLevel(boolean aLevel) {
        level = aLevel;
    }

    /**
     * Access method for title.
     *
     * @return the current value of title
     */
    public String getTitle() {
        return title;
    }

    /**
     * Setter method for title.
     *
     * @param aTitle the new value for title
     */
    public void setTitle(String aTitle) {
        title = aTitle;
    }

    /**
     * Access method for description.
     *
     * @return the current value of description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Setter method for description.
     *
     * @param aDescription the new value for description
     */
    public void setDescription(String aDescription) {
        description = aDescription;
    }

    /**
     * Access method for type.
     *
     * @return the current value of type
     */
    public String getType() {
        return type;
    }

    /**
     * Setter method for type.
     *
     * @param aType the new value for type
     */
    public void setType(String aType) {
        type = aType;
    }

    /**
     * Access method for expire.
     *
     * @return the current value of expire
     */
    public LocalDateTime getExpire() {
        return expire;
    }

    /**
     * Setter method for expire.
     *
     * @param aExpire the new value for expire
     */
    public void setExpire(LocalDateTime aExpire) {
        expire = aExpire;
    }

    /**
     * Access method for actions.
     *
     * @return the current value of actions
     */
    public String getActions() {
        return actions;
    }

    /**
     * Setter method for actions.
     *
     * @param aActions the new value for actions
     */
    public void setActions(String aActions) {
        actions = aActions;
    }

    /**
     * Access method for alternativeActions.
     *
     * @return the current value of alternativeActions
     */
    public String getAlternativeActions() {
        return alternativeActions;
    }

    /**
     * Setter method for alternativeActions.
     *
     * @param aAlternativeActions the new value for alternativeActions
     */
    public void setAlternativeActions(String aAlternativeActions) {
        alternativeActions = aAlternativeActions;
    }

    /**
     * Access method for messageRole.
     *
     * @return the current value of messageRole
     */
    public String getMessageRole() {
        return messageRole;
    }

    /**
     * Setter method for messageRole.
     *
     * @param aMessageRole the new value for messageRole
     */
    public void setMessageRole(String aMessageRole) {
        messageRole = aMessageRole;
    }

    /**
     * Access method for actionRole.
     *
     * @return the current value of actionRole
     */
    public String getActionRole() {
        return actionRole;
    }

    /**
     * Setter method for actionRole.
     *
     * @param aActionRole the new value for actionRole
     */
    public void setActionRole(String aActionRole) {
        actionRole = aActionRole;
    }

    /**
     * Access method for source.
     *
     * @return the current value of source
     */
    public String getSource() {
        return source;
    }

    /**
     * Setter method for source.
     *
     * @param aSource the new value for source
     */
    public void setSource(String aSource) {
        source = aSource;
    }

    /**
     * Compares the key for this instance with another StatusMessage.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class StatusMessage and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof StatusMessage)) {
            return false;
        }
        StatusMessage that = (StatusMessage) other;
        Object myId = this.getId();
        Object yourId = that.getId();
        if (myId==null ? yourId!=null : !myId.equals(yourId)) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another StatusMessage.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof StatusMessage)) return false;
        return this.equalKeys(other) && ((StatusMessage)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        if (getId() == null) {
            i = 0;
        } else {
            i = getId().hashCode();
        }
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[StatusMessage |");
        sb.append(" id=").append(getId());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("id", getId());
        return ret;
    }

}
