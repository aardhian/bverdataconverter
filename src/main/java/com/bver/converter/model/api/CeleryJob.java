// Generated with g9.

package com.bver.converter.model.api;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Version;

@Entity(name="celery_job")
public class CeleryJob implements Serializable {

    /** Primary key. */
    protected static final String PK = "id";

    /**
     * The optimistic lock. Available via standard bean get/set operations.
     */
    @Version
    @Column(name="LOCK_FLAG")
    private Integer lockFlag;

    /**
     * Access method for the lockFlag property.
     *
     * @return the current value of the lockFlag property
     */
    public Integer getLockFlag() {
        return lockFlag;
    }

    /**
     * Sets the value of the lockFlag property.
     *
     * @param aLockFlag the new value of the lockFlag property
     */
    public void setLockFlag(Integer aLockFlag) {
        lockFlag = aLockFlag;
    }

    @Id
    @Column(unique=true, nullable=false, length=16)
    private byte[] id;
    private byte[] info;
    @Column(name="last_modified")
    private LocalDateTime lastModified;

    /** Default constructor. */
    public CeleryJob() {
        super();
    }

    /**
     * Access method for id.
     *
     * @return the current value of id
     */
    public byte[] getId() {
        return id;
    }

    /**
     * Setter method for id.
     *
     * @param aId the new value for id
     */
    public void setId(byte[] aId) {
        id = aId;
    }

    /**
     * Access method for info.
     *
     * @return the current value of info
     */
    public byte[] getInfo() {
        return info;
    }

    /**
     * Setter method for info.
     *
     * @param aInfo the new value for info
     */
    public void setInfo(byte[] aInfo) {
        info = aInfo;
    }

    /**
     * Access method for lastModified.
     *
     * @return the current value of lastModified
     */
    public LocalDateTime getLastModified() {
        return lastModified;
    }

    /**
     * Setter method for lastModified.
     *
     * @param aLastModified the new value for lastModified
     */
    public void setLastModified(LocalDateTime aLastModified) {
        lastModified = aLastModified;
    }

    /**
     * Compares the key for this instance with another CeleryJob.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class CeleryJob and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof CeleryJob)) {
            return false;
        }
        CeleryJob that = (CeleryJob) other;
        Object myId = this.getId();
        Object yourId = that.getId();
        if (myId==null ? yourId!=null : !myId.equals(yourId)) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another CeleryJob.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof CeleryJob)) return false;
        return this.equalKeys(other) && ((CeleryJob)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        if (getId() == null) {
            i = 0;
        } else {
            i = getId().hashCode();
        }
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[CeleryJob |");
        sb.append(" id=").append(getId());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("id", getId());
        return ret;
    }

}
