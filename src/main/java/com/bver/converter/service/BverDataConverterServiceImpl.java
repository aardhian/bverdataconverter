package com.bver.converter.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bver.converter.model.bver.EventsBySensorDs;
import com.bver.converter.model.bver.EventsCategories;
import com.bver.converter.model.bver.EventsSecTrenLastDay;
import com.bver.converter.model.bver.EventsSecTrenLastWeek;
import com.bver.converter.model.bver.EventsSiemVsLogger;
import com.bver.converter.model.vo.EventsBySensorDsVo;
import com.bver.converter.model.vo.EventsCategoriesVo;
import com.bver.converter.model.vo.EventsSecTrenLastDayVo;
import com.bver.converter.model.vo.EventsSecTrenLastWeekVo;
import com.bver.converter.model.vo.EventsSiemVsLoggerVo;
import com.bver.converter.repo.alienvault.PluginRepo;
import com.bver.converter.repo.bver.EventsBySensorDsRepo;
import com.bver.converter.repo.bver.EventsCategoriesRepo;
import com.bver.converter.repo.bver.EventsSecTrenLastDayRepo;
import com.bver.converter.repo.bver.EventsSecTrenLastWeekRepo;
import com.bver.converter.repo.bver.EventsSiemVsLoggerRepo;

@Service
public class BverDataConverterServiceImpl implements BverDataConverterService{
	private static Logger log = LoggerFactory.getLogger(BverDataConverterServiceImpl.class);
	
	@Autowired
    PluginRepo pluginRepo;
	
	@Autowired
    EventsBySensorDsRepo eventsBySensorDsRepo;
	
	@Autowired
	EventsCategoriesRepo eventsCategoriesRepo;
	
	@Autowired
	EventsSecTrenLastDayRepo eventsSecTrenLastDayRepo;
	
	@Autowired
	EventsSecTrenLastWeekRepo eventsSecTrenLastWeekRepo;
	
	@Autowired
	EventsSiemVsLoggerRepo eventsSiemVsLoggerRepo;

    @Autowired
    EntityManagerFactory emf;

	@SuppressWarnings("unchecked")
	private List<EventsBySensorDsVo> listEventsBySensorDsVo() {
		log.info("execute listEventsBySensorDsVo");
		
		EntityManager em = null;
		List<EventsBySensorDsVo> list = new ArrayList<EventsBySensorDsVo>();
		
		try {
			em = emf.createEntityManager();
			
			StringBuffer queryBuffer = new StringBuffer();
			queryBuffer.append("SELECT DISTINCT device_id, plugin_id, name, sum(acid_event.cnt) as event_cnt FROM alienvault.plugin, alienvault_siem.ac_acid_event as acid_event "
					+ "	WHERE plugin.id=acid_event.plugin_id AND device_id IN ('1000000','1000001','1000002','1000003','1000004') GROUP BY device_id, plugin_id ORDER BY event_cnt DESC");
			log.info(queryBuffer.toString());
			
			Query query = em.createNativeQuery(queryBuffer.toString());
			
			List<Object[]> listObjs = query.getResultList();
			log.info("listObjs size = "+listObjs.size());
			for(Object[] obj : listObjs) {
				
				EventsBySensorDsVo eventsBySensorDsVo = new EventsBySensorDsVo();
				eventsBySensorDsVo.setCreatedDate(new Date());
				eventsBySensorDsVo.setDeviceId(String.valueOf((Integer) obj[0]));
				eventsBySensorDsVo.setPluginId(String.valueOf((Integer) obj[1]));
				eventsBySensorDsVo.setName((String) obj[2]);
				eventsBySensorDsVo.setEventCnt(((BigDecimal) obj[3]).longValue());
				
				list.add(eventsBySensorDsVo);
			}
			log.debug("List<EventsBySensorDsVo> size :"+list.size());
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (em != null) {
				em.close();
			}else {
				log.error("createEntityManager is null");
			}
		}

        return list;
	}
	
	@SuppressWarnings("unchecked")
	private List<EventsCategoriesVo> listEventsCategoriesVo() {
		log.info("execute listEventsCategoriesVo");
		
		EntityManager em = null;
		List<EventsCategoriesVo> list = new ArrayList<EventsCategoriesVo>();
		
		try {
			em = emf.createEntityManager();
			
			StringBuffer queryBuffer = new StringBuffer();
			queryBuffer.append("SELECT sum( acid_event.cnt ) as num_events,p.category_id, ct.name FROM alienvault_siem.ac_acid_event as acid_event, alienvault.plugin_sid p, alienvault.category ct "
					+ "WHERE p.plugin_id=acid_event.plugin_id AND p.sid=acid_event.plugin_sid AND p.category_id = ct.id AND acid_event.timestamp "
					+ "BETWEEN (NOW() + INTERVAL -7 DAY) AND (NOW() + INTERVAL  0 DAY) group by p.category_id having num_events > 0 and p.category_id is not null "
					+ "order by num_events desc LIMIT 10");
			log.info(queryBuffer.toString());
			
			Query query = em.createNativeQuery(queryBuffer.toString());
			
			List<Object[]> listObjs = query.getResultList();
			log.info("listObjs size = "+listObjs.size());
			for(Object[] obj : listObjs) {
				
				EventsCategoriesVo eventsCategoriesVo = new EventsCategoriesVo();
				eventsCategoriesVo.setCreatedDate(new Date());
				eventsCategoriesVo.setCategoryId(String.valueOf((Integer) obj[1]));
				eventsCategoriesVo.setName((String) obj[2]);
				eventsCategoriesVo.setEventCnt(((BigDecimal) obj[0]).longValue());
				
				list.add(eventsCategoriesVo);
			}
			log.debug("List<EventsCategoriesVo> size :"+list.size());
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (em != null) {
				em.close();
			}else {
				log.error("createEntityManager is null");
			}
		}

        return list;
	}
	
	@SuppressWarnings("unchecked")
	private List<EventsSecTrenLastDayVo> listEventsSecTrenLastDayVo() {
		log.info("execute listEventsSecTrenLastDayVo");
		
		EntityManager em = null;
		List<EventsSecTrenLastDayVo> list = new ArrayList<EventsSecTrenLastDayVo>();
		
		try {
			em = emf.createEntityManager();
			
			StringBuffer queryBuffer = new StringBuffer();
			queryBuffer.append("SELECT SUM(cnt) AS num_events, hour(convert_tz(timestamp,'+00:00','+7:00')) AS intervalo, day(convert_tz(timestamp,'+00:00','+7:00')) AS suf "
					+ "FROM alienvault_siem.ac_acid_event as acid_event WHERE 1=1 AND (timestamp BETWEEN (NOW() + INTERVAL -7 DAY) AND (NOW() + INTERVAL  0 DAY)) GROUP BY suf,intervalo");
			log.info(queryBuffer.toString());
			
			Query query = em.createNativeQuery(queryBuffer.toString());
			
			List<Object[]> listObjs = query.getResultList();
			log.info("listObjs size = "+listObjs.size());
			for(Object[] obj : listObjs) {
				
				EventsSecTrenLastDayVo eventsSecTrenLastDayVo = new EventsSecTrenLastDayVo();
				eventsSecTrenLastDayVo.setCreatedDate(new Date());
				eventsSecTrenLastDayVo.setIntervalo((obj[1] instanceof java.lang.Integer)?String.valueOf((Integer) obj[1]):(String) obj[1]);
				eventsSecTrenLastDayVo.setSuf((obj[2] instanceof java.lang.Integer)?String.valueOf((Integer) obj[2]):(String) obj[2]);
				eventsSecTrenLastDayVo.setEventCnt(((BigDecimal) obj[0]).longValue());
				
				list.add(eventsSecTrenLastDayVo);
			}
			log.debug("List<EventsSecTrenLastDayVo> size :"+list.size());
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (em != null) {
				em.close();
			}else {
				log.error("createEntityManager is null");
			}
		}

        return list;
	}
	
	@SuppressWarnings("unchecked")
	private List<EventsSecTrenLastWeekVo> listEventsSecTrenLastWeekVo() {
		log.info("execute listEventsSecTrenLastWeekVo");
		
		EntityManager em = null;
		List<EventsSecTrenLastWeekVo> list = new ArrayList<EventsSecTrenLastWeekVo>();
		
		try {
			em = emf.createEntityManager();
			
			StringBuffer queryBuffer = new StringBuffer();
			queryBuffer.append("SELECT SUM(acid_event.cnt) AS num_events, day(convert_tz(timestamp,'+00:00','+7:00')) AS intervalo, monthname(convert_tz(timestamp,'+00:00','+7:00')) AS suf "
					+ "FROM alienvault_siem.ac_acid_event as acid_event WHERE 1=1 AND (timestamp BETWEEN (NOW() + INTERVAL -7 DAY) AND (NOW() + INTERVAL  0 DAY)) GROUP BY suf, intervalo ORDER BY suf, intervalo");
			log.info(queryBuffer.toString());
			
			Query query = em.createNativeQuery(queryBuffer.toString());
			
			List<Object[]> listObjs = query.getResultList();
			log.info("listObjs size = "+listObjs.size());
			for(Object[] obj : listObjs) {
				
				EventsSecTrenLastWeekVo eventsSecTrenLastWeekVo = new EventsSecTrenLastWeekVo();
				eventsSecTrenLastWeekVo.setCreatedDate(new Date());
				eventsSecTrenLastWeekVo.setIntervalo((obj[1] instanceof java.lang.Integer)?String.valueOf((Integer) obj[1]):(String) obj[1]);
				eventsSecTrenLastWeekVo.setSuf((obj[2] instanceof java.lang.Integer)?String.valueOf((Integer) obj[2]):(String) obj[2]);
				eventsSecTrenLastWeekVo.setEventCnt(((BigDecimal) obj[0]).longValue());
				
				list.add(eventsSecTrenLastWeekVo);
			}
			log.debug("List<EventsSecTrenLastWeekVo> size :"+list.size());
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (em != null) {
				em.close();
			}else {
				log.error("createEntityManager is null");
			}
		}

        return list;
	}
	
	@SuppressWarnings("unchecked")
	private List<EventsSiemVsLoggerVo> listEventsSiemVsLoggerVo() {
		log.info("execute listEventsSiemVsLoggerVo");
		
		EntityManager em = null;
		List<EventsSiemVsLoggerVo> list = new ArrayList<EventsSiemVsLoggerVo>();
		
		try {
			em = emf.createEntityManager();
			
			StringBuffer queryBuffer = new StringBuffer();
			queryBuffer.append("SELECT SUM(cnt) AS num_events, hour(convert_tz(timestamp,'+00:00','+7:00')) AS intervalo, day(convert_tz(timestamp,'+00:00','+7:00')) AS suf "
					+ "FROM alienvault_siem.ac_acid_event as acid_event WHERE 1=1 AND (timestamp BETWEEN (NOW() + INTERVAL -7 DAY) AND (NOW() + INTERVAL  0 DAY)) GROUP BY suf,intervalo");
			log.info(queryBuffer.toString());
			
			Query query = em.createNativeQuery(queryBuffer.toString());
			
			List<Object[]> listObjs = query.getResultList();
			log.info("listObjs size = "+listObjs.size());
			for(Object[] obj : listObjs) {
				
				EventsSiemVsLoggerVo eventsSiemVsLoggerVo = new EventsSiemVsLoggerVo();
				eventsSiemVsLoggerVo.setCreatedDate(new Date());
				eventsSiemVsLoggerVo.setIntervalo((obj[1] instanceof java.lang.Integer)?String.valueOf((Integer) obj[1]):(String) obj[1]);
				eventsSiemVsLoggerVo.setSuf((obj[2] instanceof java.lang.Integer)?String.valueOf((Integer) obj[2]):(String) obj[2]);
				eventsSiemVsLoggerVo.setEventCnt(((BigDecimal) obj[0]).longValue());
				
				list.add(eventsSiemVsLoggerVo);
			}
			log.debug("List<EventsSiemVsLoggerVo> size :"+list.size());
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (em != null) {
				em.close();
			}else {
				log.error("createEntityManager is null");
			}
		}

        return list;
	}

	@Override
	public void executeEventsBySensorDs() {
		log.info("executeEventsBySensorDs");
		
		List<EventsBySensorDsVo> list = listEventsBySensorDsVo();

		for (EventsBySensorDsVo eventsBySensorDsVo : list) {
			EventsBySensorDs eventsBySensorDs = new EventsBySensorDs();
			eventsBySensorDs.setCreatedDate(eventsBySensorDsVo.getCreatedDate());
			eventsBySensorDs.setDeviceId(eventsBySensorDsVo.getDeviceId());
			eventsBySensorDs.setPluginId(eventsBySensorDsVo.getPluginId());
			eventsBySensorDs.setName(eventsBySensorDsVo.getName());
			eventsBySensorDs.setEventCnt(eventsBySensorDs.getEventCnt());
			
			try {
				eventsBySensorDsRepo.save(eventsBySensorDs);
			} catch (Exception e) {
				log.error(e.getMessage());
			}
		}
	}

	@Override
	public void executeEventsCategories() {
		log.info("executeEventsCategories");
		
		List<EventsCategoriesVo> list = listEventsCategoriesVo();

		for (EventsCategoriesVo eventsCategoriesVo : list) {
			EventsCategories eventsCategories = new EventsCategories();
			eventsCategories.setCreatedDate(eventsCategoriesVo.getCreatedDate());
			eventsCategories.setCategoryId(eventsCategoriesVo.getCategoryId());
			eventsCategories.setName(eventsCategoriesVo.getName());
			eventsCategories.setEventCnt(eventsCategoriesVo.getEventCnt());
			
			try {
				eventsCategoriesRepo.save(eventsCategories);
			} catch (Exception e) {
				log.error(e.getMessage());
			}
		}
	}

	@Override
	public void executeEventsSecTrenLastDay() {
		log.info("executeEventsSecTrenLastDay");
		
		List<EventsSecTrenLastDayVo> list = listEventsSecTrenLastDayVo();

		for (EventsSecTrenLastDayVo eventsSecTrenLastDayVo : list) {
			EventsSecTrenLastDay eventsSecTrenLastDay = new EventsSecTrenLastDay();
			eventsSecTrenLastDay.setCreatedDate(eventsSecTrenLastDayVo.getCreatedDate());
			eventsSecTrenLastDay.setIntervalo(eventsSecTrenLastDayVo.getIntervalo());
			eventsSecTrenLastDay.setSuf(eventsSecTrenLastDayVo.getSuf());
			eventsSecTrenLastDay.setEventCnt(eventsSecTrenLastDayVo.getEventCnt());
			
			try {
				eventsSecTrenLastDayRepo.save(eventsSecTrenLastDay);
			} catch (Exception e) {
				log.error(e.getMessage());
			}
		}
	}

	@Override
	public void executeEventsSecTrenLastWeek() {
		log.info("executeEventsSecTrenLastWeek");
		
		List<EventsSecTrenLastWeekVo> list = listEventsSecTrenLastWeekVo();

		for (EventsSecTrenLastWeekVo eventsSecTrenLastWeekVo : list) {
			EventsSecTrenLastWeek eventsSecTrenLastWeek = new EventsSecTrenLastWeek();
			eventsSecTrenLastWeek.setCreatedDate(eventsSecTrenLastWeekVo.getCreatedDate());
			eventsSecTrenLastWeek.setIntervalo(eventsSecTrenLastWeekVo.getIntervalo());
			eventsSecTrenLastWeek.setSuf(eventsSecTrenLastWeekVo.getSuf());
			eventsSecTrenLastWeek.setEventCnt(eventsSecTrenLastWeekVo.getEventCnt());
			
			try {
				eventsSecTrenLastWeekRepo.save(eventsSecTrenLastWeek);
			} catch (Exception e) {
				log.error(e.getMessage());
			}
		}
	}

	@Override
	public void executeEventsSiemVsLogger() {
		log.info("executeEventsSiemVsLogger");
		
		List<EventsSiemVsLoggerVo> list = listEventsSiemVsLoggerVo();

		for (EventsSiemVsLoggerVo eventsSiemVsLoggerVo : list) {
			EventsSiemVsLogger eventsSiemVsLogger = new EventsSiemVsLogger();
			eventsSiemVsLogger.setCreatedDate(eventsSiemVsLoggerVo.getCreatedDate());
			eventsSiemVsLogger.setIntervalo(eventsSiemVsLoggerVo.getIntervalo());
			eventsSiemVsLogger.setSuf(eventsSiemVsLoggerVo.getSuf());
			eventsSiemVsLogger.setEventCnt(eventsSiemVsLoggerVo.getEventCnt());
			
			try {
				eventsSiemVsLoggerRepo.save(eventsSiemVsLogger);
			} catch (Exception e) {
				log.error(e.getMessage());
			}
		}
	}

}
