package com.bver.converter.repo.bver;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bver.converter.model.bver.EventsSecTrenLastDay;

@Repository
public interface EventsSecTrenLastDayRepo extends JpaRepository<EventsSecTrenLastDay, Long> {

	@Override
    public List<EventsSecTrenLastDay> findAll();

}
