// Generated with g9.

package com.bver.converter.model.api;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Version;

@Entity(name="current_status")
public class CurrentStatus implements Serializable {

    /** Primary key. */
    protected static final String PK = "id";

    /**
     * The optimistic lock. Available via standard bean get/set operations.
     */
    @Version
    @Column(name="LOCK_FLAG")
    private Integer lockFlag;

    /**
     * Access method for the lockFlag property.
     *
     * @return the current value of the lockFlag property
     */
    public Integer getLockFlag() {
        return lockFlag;
    }

    /**
     * Sets the value of the lockFlag property.
     *
     * @param aLockFlag the new value of the lockFlag property
     */
    public void setLockFlag(Integer aLockFlag) {
        lockFlag = aLockFlag;
    }

    @Id
    @Column(unique=true, nullable=false, length=16)
    private byte[] id;
    @Column(name="message_id", nullable=false, length=16)
    private byte[] messageId;
    @Column(name="component_id", length=16)
    private byte[] componentId;
    @Column(name="component_type", nullable=false, length=8)
    private String componentType;
    @Column(name="creation_time", nullable=false)
    private LocalDateTime creationTime;
    @Column(length=3)
    private boolean viewed;
    @Column(length=3)
    private boolean suppressed;
    @Column(name="suppressed_time")
    private LocalDateTime suppressedTime;
    @Column(name="additional_info")
    private String additionalInfo;

    /** Default constructor. */
    public CurrentStatus() {
        super();
    }

    /**
     * Access method for id.
     *
     * @return the current value of id
     */
    public byte[] getId() {
        return id;
    }

    /**
     * Setter method for id.
     *
     * @param aId the new value for id
     */
    public void setId(byte[] aId) {
        id = aId;
    }

    /**
     * Access method for messageId.
     *
     * @return the current value of messageId
     */
    public byte[] getMessageId() {
        return messageId;
    }

    /**
     * Setter method for messageId.
     *
     * @param aMessageId the new value for messageId
     */
    public void setMessageId(byte[] aMessageId) {
        messageId = aMessageId;
    }

    /**
     * Access method for componentId.
     *
     * @return the current value of componentId
     */
    public byte[] getComponentId() {
        return componentId;
    }

    /**
     * Setter method for componentId.
     *
     * @param aComponentId the new value for componentId
     */
    public void setComponentId(byte[] aComponentId) {
        componentId = aComponentId;
    }

    /**
     * Access method for componentType.
     *
     * @return the current value of componentType
     */
    public String getComponentType() {
        return componentType;
    }

    /**
     * Setter method for componentType.
     *
     * @param aComponentType the new value for componentType
     */
    public void setComponentType(String aComponentType) {
        componentType = aComponentType;
    }

    /**
     * Access method for creationTime.
     *
     * @return the current value of creationTime
     */
    public LocalDateTime getCreationTime() {
        return creationTime;
    }

    /**
     * Setter method for creationTime.
     *
     * @param aCreationTime the new value for creationTime
     */
    public void setCreationTime(LocalDateTime aCreationTime) {
        creationTime = aCreationTime;
    }

    /**
     * Access method for viewed.
     *
     * @return true if and only if viewed is currently true
     */
    public boolean getViewed() {
        return viewed;
    }

    /**
     * Setter method for viewed.
     *
     * @param aViewed the new value for viewed
     */
    public void setViewed(boolean aViewed) {
        viewed = aViewed;
    }

    /**
     * Access method for suppressed.
     *
     * @return true if and only if suppressed is currently true
     */
    public boolean getSuppressed() {
        return suppressed;
    }

    /**
     * Setter method for suppressed.
     *
     * @param aSuppressed the new value for suppressed
     */
    public void setSuppressed(boolean aSuppressed) {
        suppressed = aSuppressed;
    }

    /**
     * Access method for suppressedTime.
     *
     * @return the current value of suppressedTime
     */
    public LocalDateTime getSuppressedTime() {
        return suppressedTime;
    }

    /**
     * Setter method for suppressedTime.
     *
     * @param aSuppressedTime the new value for suppressedTime
     */
    public void setSuppressedTime(LocalDateTime aSuppressedTime) {
        suppressedTime = aSuppressedTime;
    }

    /**
     * Access method for additionalInfo.
     *
     * @return the current value of additionalInfo
     */
    public String getAdditionalInfo() {
        return additionalInfo;
    }

    /**
     * Setter method for additionalInfo.
     *
     * @param aAdditionalInfo the new value for additionalInfo
     */
    public void setAdditionalInfo(String aAdditionalInfo) {
        additionalInfo = aAdditionalInfo;
    }

    /**
     * Compares the key for this instance with another CurrentStatus.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class CurrentStatus and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof CurrentStatus)) {
            return false;
        }
        CurrentStatus that = (CurrentStatus) other;
        Object myId = this.getId();
        Object yourId = that.getId();
        if (myId==null ? yourId!=null : !myId.equals(yourId)) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another CurrentStatus.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof CurrentStatus)) return false;
        return this.equalKeys(other) && ((CurrentStatus)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        if (getId() == null) {
            i = 0;
        } else {
            i = getId().hashCode();
        }
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[CurrentStatus |");
        sb.append(" id=").append(getId());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("id", getId());
        return ret;
    }

}
