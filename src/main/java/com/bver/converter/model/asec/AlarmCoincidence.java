// Generated with g9.

package com.bver.converter.model.asec;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Version;

@Entity(name="alarm_coincidence")
public class AlarmCoincidence implements Serializable {

    /** Primary key. */
    protected static final String PK = "id";

    /**
     * The optimistic lock. Available via standard bean get/set operations.
     */
    @Version
    @Column(name="LOCK_FLAG")
    private Integer lockFlag;

    /**
     * Access method for the lockFlag property.
     *
     * @return the current value of the lockFlag property
     */
    public Integer getLockFlag() {
        return lockFlag;
    }

    /**
     * Sets the value of the lockFlag property.
     *
     * @param aLockFlag the new value of the lockFlag property
     */
    public void setLockFlag(Integer aLockFlag) {
        lockFlag = aLockFlag;
    }

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(unique=true, nullable=false, precision=19)
    private long id;
    @Column(nullable=false)
    private String data;
    @Column(name="sample_log", nullable=false)
    private String sampleLog;
    @Column(name="sensor_id", nullable=false, length=16)
    private byte[] sensorId;
    @Column(nullable=false)
    private LocalDateTime datetime;

    /** Default constructor. */
    public AlarmCoincidence() {
        super();
    }

    /**
     * Access method for id.
     *
     * @return the current value of id
     */
    public long getId() {
        return id;
    }

    /**
     * Setter method for id.
     *
     * @param aId the new value for id
     */
    public void setId(long aId) {
        id = aId;
    }

    /**
     * Access method for data.
     *
     * @return the current value of data
     */
    public String getData() {
        return data;
    }

    /**
     * Setter method for data.
     *
     * @param aData the new value for data
     */
    public void setData(String aData) {
        data = aData;
    }

    /**
     * Access method for sampleLog.
     *
     * @return the current value of sampleLog
     */
    public String getSampleLog() {
        return sampleLog;
    }

    /**
     * Setter method for sampleLog.
     *
     * @param aSampleLog the new value for sampleLog
     */
    public void setSampleLog(String aSampleLog) {
        sampleLog = aSampleLog;
    }

    /**
     * Access method for sensorId.
     *
     * @return the current value of sensorId
     */
    public byte[] getSensorId() {
        return sensorId;
    }

    /**
     * Setter method for sensorId.
     *
     * @param aSensorId the new value for sensorId
     */
    public void setSensorId(byte[] aSensorId) {
        sensorId = aSensorId;
    }

    /**
     * Access method for datetime.
     *
     * @return the current value of datetime
     */
    public LocalDateTime getDatetime() {
        return datetime;
    }

    /**
     * Setter method for datetime.
     *
     * @param aDatetime the new value for datetime
     */
    public void setDatetime(LocalDateTime aDatetime) {
        datetime = aDatetime;
    }

    /**
     * Compares the key for this instance with another AlarmCoincidence.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class AlarmCoincidence and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof AlarmCoincidence)) {
            return false;
        }
        AlarmCoincidence that = (AlarmCoincidence) other;
        if (this.getId() != that.getId()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another AlarmCoincidence.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof AlarmCoincidence)) return false;
        return this.equalKeys(other) && ((AlarmCoincidence)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = (int)(getId() ^ (getId()>>>32));
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[AlarmCoincidence |");
        sb.append(" id=").append(getId());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("id", Long.valueOf(getId()));
        return ret;
    }

}
