// Generated with g9.

package com.bver.converter.model.api;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Version;

@Entity(name="monitor_data")
@IdClass(MonitorData.MonitorDataId.class)
public class MonitorData implements Serializable {

    /**
     * IdClass for primary key when using JPA annotations
     */
    public class MonitorDataId implements Serializable {
        byte[] componentId;
        java.time.LocalDateTime timestamp;
        int monitorId;
    }

    /** Primary key. */
    protected static final String PK = "MonitorDataPrimary";

    /**
     * The optimistic lock. Available via standard bean get/set operations.
     */
    @Version
    @Column(name="LOCK_FLAG")
    private Integer lockFlag;

    /**
     * Access method for the lockFlag property.
     *
     * @return the current value of the lockFlag property
     */
    public Integer getLockFlag() {
        return lockFlag;
    }

    /**
     * Sets the value of the lockFlag property.
     *
     * @param aLockFlag the new value of the lockFlag property
     */
    public void setLockFlag(Integer aLockFlag) {
        lockFlag = aLockFlag;
    }

    @Id
    @Column(name="component_id", nullable=false, length=16)
    private byte[] componentId;
    @Id
    @Column(nullable=false)
    private LocalDateTime timestamp;
    @Id
    @Column(name="monitor_id", nullable=false, precision=10)
    private int monitorId;
    private String data;
    @Column(name="component_type", length=55)
    private String componentType;

    /** Default constructor. */
    public MonitorData() {
        super();
    }

    /**
     * Access method for componentId.
     *
     * @return the current value of componentId
     */
    public byte[] getComponentId() {
        return componentId;
    }

    /**
     * Setter method for componentId.
     *
     * @param aComponentId the new value for componentId
     */
    public void setComponentId(byte[] aComponentId) {
        componentId = aComponentId;
    }

    /**
     * Access method for timestamp.
     *
     * @return the current value of timestamp
     */
    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    /**
     * Setter method for timestamp.
     *
     * @param aTimestamp the new value for timestamp
     */
    public void setTimestamp(LocalDateTime aTimestamp) {
        timestamp = aTimestamp;
    }

    /**
     * Access method for monitorId.
     *
     * @return the current value of monitorId
     */
    public int getMonitorId() {
        return monitorId;
    }

    /**
     * Setter method for monitorId.
     *
     * @param aMonitorId the new value for monitorId
     */
    public void setMonitorId(int aMonitorId) {
        monitorId = aMonitorId;
    }

    /**
     * Access method for data.
     *
     * @return the current value of data
     */
    public String getData() {
        return data;
    }

    /**
     * Setter method for data.
     *
     * @param aData the new value for data
     */
    public void setData(String aData) {
        data = aData;
    }

    /**
     * Access method for componentType.
     *
     * @return the current value of componentType
     */
    public String getComponentType() {
        return componentType;
    }

    /**
     * Setter method for componentType.
     *
     * @param aComponentType the new value for componentType
     */
    public void setComponentType(String aComponentType) {
        componentType = aComponentType;
    }

    /**
     * Compares the key for this instance with another MonitorData.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class MonitorData and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof MonitorData)) {
            return false;
        }
        MonitorData that = (MonitorData) other;
        Object myComponentId = this.getComponentId();
        Object yourComponentId = that.getComponentId();
        if (myComponentId==null ? yourComponentId!=null : !myComponentId.equals(yourComponentId)) {
            return false;
        }
        Object myTimestamp = this.getTimestamp();
        Object yourTimestamp = that.getTimestamp();
        if (myTimestamp==null ? yourTimestamp!=null : !myTimestamp.equals(yourTimestamp)) {
            return false;
        }
        if (this.getMonitorId() != that.getMonitorId()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another MonitorData.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof MonitorData)) return false;
        return this.equalKeys(other) && ((MonitorData)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        if (getComponentId() == null) {
            i = 0;
        } else {
            i = getComponentId().hashCode();
        }
        result = 37*result + i;
        if (getTimestamp() == null) {
            i = 0;
        } else {
            i = getTimestamp().hashCode();
        }
        result = 37*result + i;
        i = getMonitorId();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[MonitorData |");
        sb.append(" componentId=").append(getComponentId());
        sb.append(" timestamp=").append(getTimestamp());
        sb.append(" monitorId=").append(getMonitorId());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("componentId", getComponentId());
        ret.put("timestamp", getTimestamp());
        ret.put("monitorId", Integer.valueOf(getMonitorId()));
        return ret;
    }

}
