// Generated with g9.

package com.bver.converter.model;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Version;

@Entity(name="ac_acid_event")
public class AcAcidEvent implements Serializable {

    /**
     * The optimistic lock. Available via standard bean get/set operations.
     */
    @Version
    @Column(name="LOCK_FLAG")
    private Integer lockFlag;

    /**
     * Access method for the lockFlag property.
     *
     * @return the current value of the lockFlag property
     */
    public Integer getLockFlag() {
        return lockFlag;
    }

    /**
     * Sets the value of the lockFlag property.
     *
     * @param aLockFlag the new value of the lockFlag property
     */
    public void setLockFlag(Integer aLockFlag) {
        lockFlag = aLockFlag;
    }

    @Column(nullable=false, length=16)
    private byte[] ctx;
    @Id
    @Column(name="device_id", nullable=false, precision=10)
    private int deviceId;
    @Column(name="plugin_id", nullable=false, precision=10)
    private int pluginId;
    @Column(name="plugin_sid", nullable=false, precision=10)
    private int pluginSid;
    @Column(nullable=false)
    private LocalDateTime timestamp;
    @Column(name="src_host", nullable=false, length=16)
    private byte[] srcHost;
    @Column(name="dst_host", nullable=false, length=16)
    private byte[] dstHost;
    @Column(name="src_net", nullable=false, length=16)
    private byte[] srcNet;
    @Column(name="dst_net", nullable=false, length=16)
    private byte[] dstNet;
    @Column(nullable=false, precision=10)
    private int cnt;

    /** Default constructor. */
    public AcAcidEvent() {
        super();
    }

    /**
     * Access method for ctx.
     *
     * @return the current value of ctx
     */
    public byte[] getCtx() {
        return ctx;
    }

    /**
     * Setter method for ctx.
     *
     * @param aCtx the new value for ctx
     */
    public void setCtx(byte[] aCtx) {
        ctx = aCtx;
    }

    /**
     * Access method for deviceId.
     *
     * @return the current value of deviceId
     */
    public int getDeviceId() {
        return deviceId;
    }

    /**
     * Setter method for deviceId.
     *
     * @param aDeviceId the new value for deviceId
     */
    public void setDeviceId(int aDeviceId) {
        deviceId = aDeviceId;
    }

    /**
     * Access method for pluginId.
     *
     * @return the current value of pluginId
     */
    public int getPluginId() {
        return pluginId;
    }

    /**
     * Setter method for pluginId.
     *
     * @param aPluginId the new value for pluginId
     */
    public void setPluginId(int aPluginId) {
        pluginId = aPluginId;
    }

    /**
     * Access method for pluginSid.
     *
     * @return the current value of pluginSid
     */
    public int getPluginSid() {
        return pluginSid;
    }

    /**
     * Setter method for pluginSid.
     *
     * @param aPluginSid the new value for pluginSid
     */
    public void setPluginSid(int aPluginSid) {
        pluginSid = aPluginSid;
    }

    /**
     * Access method for timestamp.
     *
     * @return the current value of timestamp
     */
    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    /**
     * Setter method for timestamp.
     *
     * @param aTimestamp the new value for timestamp
     */
    public void setTimestamp(LocalDateTime aTimestamp) {
        timestamp = aTimestamp;
    }

    /**
     * Access method for srcHost.
     *
     * @return the current value of srcHost
     */
    public byte[] getSrcHost() {
        return srcHost;
    }

    /**
     * Setter method for srcHost.
     *
     * @param aSrcHost the new value for srcHost
     */
    public void setSrcHost(byte[] aSrcHost) {
        srcHost = aSrcHost;
    }

    /**
     * Access method for dstHost.
     *
     * @return the current value of dstHost
     */
    public byte[] getDstHost() {
        return dstHost;
    }

    /**
     * Setter method for dstHost.
     *
     * @param aDstHost the new value for dstHost
     */
    public void setDstHost(byte[] aDstHost) {
        dstHost = aDstHost;
    }

    /**
     * Access method for srcNet.
     *
     * @return the current value of srcNet
     */
    public byte[] getSrcNet() {
        return srcNet;
    }

    /**
     * Setter method for srcNet.
     *
     * @param aSrcNet the new value for srcNet
     */
    public void setSrcNet(byte[] aSrcNet) {
        srcNet = aSrcNet;
    }

    /**
     * Access method for dstNet.
     *
     * @return the current value of dstNet
     */
    public byte[] getDstNet() {
        return dstNet;
    }

    /**
     * Setter method for dstNet.
     *
     * @param aDstNet the new value for dstNet
     */
    public void setDstNet(byte[] aDstNet) {
        dstNet = aDstNet;
    }

    /**
     * Access method for cnt.
     *
     * @return the current value of cnt
     */
    public int getCnt() {
        return cnt;
    }

    /**
     * Setter method for cnt.
     *
     * @param aCnt the new value for cnt
     */
    public void setCnt(int aCnt) {
        cnt = aCnt;
    }

}
