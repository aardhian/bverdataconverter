package com.bver.converter.model.vo;

import java.util.Date;

public class EventsSecTrenLastDayVo {
	private long id;
    private Date createdDate;
    private String intervalo;
    private String suf;
    private long eventCnt;
    
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public String getIntervalo() {
		return intervalo;
	}
	public void setIntervalo(String intervalo) {
		this.intervalo = intervalo;
	}
	public String getSuf() {
		return suf;
	}
	public void setSuf(String suf) {
		this.suf = suf;
	}
	public long getEventCnt() {
		return eventCnt;
	}
	public void setEventCnt(long eventCnt) {
		this.eventCnt = eventCnt;
	}
    
	
}
