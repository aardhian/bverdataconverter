// Generated with g9.

package com.bver.converter.model.asec;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Version;

@Entity(name="suggestions")
public class Suggestions implements Serializable {

    /** Primary key. */
    protected static final String PK = "id";

    /**
     * The optimistic lock. Available via standard bean get/set operations.
     */
    @Version
    @Column(name="LOCK_FLAG")
    private Integer lockFlag;

    /**
     * Access method for the lockFlag property.
     *
     * @return the current value of the lockFlag property
     */
    public Integer getLockFlag() {
        return lockFlag;
    }

    /**
     * Sets the value of the lockFlag property.
     *
     * @param aLockFlag the new value of the lockFlag property
     */
    public void setLockFlag(Integer aLockFlag) {
        lockFlag = aLockFlag;
    }

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(unique=true, nullable=false, precision=19)
    private long id;
    @Column(name="suggestion_group_id", nullable=false, length=16)
    private byte[] suggestionGroupId;
    @Column(nullable=false, length=255)
    private String filename;
    @Column(nullable=false, length=255)
    private String location;
    @Column(nullable=false)
    private LocalDateTime datetime;

    /** Default constructor. */
    public Suggestions() {
        super();
    }

    /**
     * Access method for id.
     *
     * @return the current value of id
     */
    public long getId() {
        return id;
    }

    /**
     * Setter method for id.
     *
     * @param aId the new value for id
     */
    public void setId(long aId) {
        id = aId;
    }

    /**
     * Access method for suggestionGroupId.
     *
     * @return the current value of suggestionGroupId
     */
    public byte[] getSuggestionGroupId() {
        return suggestionGroupId;
    }

    /**
     * Setter method for suggestionGroupId.
     *
     * @param aSuggestionGroupId the new value for suggestionGroupId
     */
    public void setSuggestionGroupId(byte[] aSuggestionGroupId) {
        suggestionGroupId = aSuggestionGroupId;
    }

    /**
     * Access method for filename.
     *
     * @return the current value of filename
     */
    public String getFilename() {
        return filename;
    }

    /**
     * Setter method for filename.
     *
     * @param aFilename the new value for filename
     */
    public void setFilename(String aFilename) {
        filename = aFilename;
    }

    /**
     * Access method for location.
     *
     * @return the current value of location
     */
    public String getLocation() {
        return location;
    }

    /**
     * Setter method for location.
     *
     * @param aLocation the new value for location
     */
    public void setLocation(String aLocation) {
        location = aLocation;
    }

    /**
     * Access method for datetime.
     *
     * @return the current value of datetime
     */
    public LocalDateTime getDatetime() {
        return datetime;
    }

    /**
     * Setter method for datetime.
     *
     * @param aDatetime the new value for datetime
     */
    public void setDatetime(LocalDateTime aDatetime) {
        datetime = aDatetime;
    }

    /**
     * Compares the key for this instance with another Suggestions.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Suggestions and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Suggestions)) {
            return false;
        }
        Suggestions that = (Suggestions) other;
        if (this.getId() != that.getId()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Suggestions.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Suggestions)) return false;
        return this.equalKeys(other) && ((Suggestions)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = (int)(getId() ^ (getId()>>>32));
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Suggestions |");
        sb.append(" id=").append(getId());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("id", Long.valueOf(getId()));
        return ret;
    }

}
