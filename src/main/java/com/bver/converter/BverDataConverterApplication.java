package com.bver.converter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BverDataConverterApplication implements CommandLineRunner {
	private static Logger log = LoggerFactory
		      .getLogger(BverDataConverterApplication.class);

	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		log.info("STARTING THE APPLICATION");
		SpringApplication.run(BverDataConverterApplication.class, args);
		log.info("APPLICATION FINISHED " + (System.currentTimeMillis() - start) + "ms");
	}

	@Override
	public void run(String... args) throws Exception {
		log.info("EXECUTING : command line runner");
		 
        for (int i = 0; i < args.length; ++i) {
            log.info("args[{}]: {}", i, args[i]);
        }
	}

}
