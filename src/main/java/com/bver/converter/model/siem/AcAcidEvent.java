// Generated with g9.

package com.bver.converter.model.siem;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name="ac_acid_event", indexes={@Index(name="acAcidEventPluginId", columnList="plugin_id,plugin_sid")})
@IdClass(AcAcidEvent.AcAcidEventId.class)
public class AcAcidEvent implements Serializable {

    /**
     * IdClass for primary key when using JPA annotations
     */
    public class AcAcidEventId implements Serializable {
        byte[] ctx;
        int deviceId;
        int pluginId;
        int pluginSid;
        java.time.LocalDateTime timestamp;
        byte[] srcHost;
        byte[] dstHost;
        byte[] srcNet;
        byte[] dstNet;
    }

    /** Primary key. */
    protected static final String PK = "AcAcidEventPrimary";

    /**
     * The optimistic lock. Available via standard bean get/set operations.
     */
    @Version
    @Column(name="LOCK_FLAG")
    private Integer lockFlag;

    /**
     * Access method for the lockFlag property.
     *
     * @return the current value of the lockFlag property
     */
    public Integer getLockFlag() {
        return lockFlag;
    }

    /**
     * Sets the value of the lockFlag property.
     *
     * @param aLockFlag the new value of the lockFlag property
     */
    public void setLockFlag(Integer aLockFlag) {
        lockFlag = aLockFlag;
    }

    @Id
    @Column(nullable=false, length=16)
    private byte[] ctx;
    @Id
    @Column(name="device_id", nullable=false, precision=10)
    private int deviceId;
    @Id
    @Column(name="plugin_id", nullable=false, precision=10)
    private int pluginId;
    @Id
    @Column(name="plugin_sid", nullable=false, precision=10)
    private int pluginSid;
    @Id
    @Column(nullable=false)
    private LocalDateTime timestamp;
    @Id
    @Column(name="src_host", nullable=false, length=16)
    private byte[] srcHost;
    @Id
    @Column(name="dst_host", nullable=false, length=16)
    private byte[] dstHost;
    @Id
    @Column(name="src_net", nullable=false, length=16)
    private byte[] srcNet;
    @Id
    @Column(name="dst_net", nullable=false, length=16)
    private byte[] dstNet;
    @Column(nullable=false, precision=10)
    private int cnt;

    /** Default constructor. */
    public AcAcidEvent() {
        super();
    }

    /**
     * Access method for ctx.
     *
     * @return the current value of ctx
     */
    public byte[] getCtx() {
        return ctx;
    }

    /**
     * Setter method for ctx.
     *
     * @param aCtx the new value for ctx
     */
    public void setCtx(byte[] aCtx) {
        ctx = aCtx;
    }

    /**
     * Access method for deviceId.
     *
     * @return the current value of deviceId
     */
    public int getDeviceId() {
        return deviceId;
    }

    /**
     * Setter method for deviceId.
     *
     * @param aDeviceId the new value for deviceId
     */
    public void setDeviceId(int aDeviceId) {
        deviceId = aDeviceId;
    }

    /**
     * Access method for pluginId.
     *
     * @return the current value of pluginId
     */
    public int getPluginId() {
        return pluginId;
    }

    /**
     * Setter method for pluginId.
     *
     * @param aPluginId the new value for pluginId
     */
    public void setPluginId(int aPluginId) {
        pluginId = aPluginId;
    }

    /**
     * Access method for pluginSid.
     *
     * @return the current value of pluginSid
     */
    public int getPluginSid() {
        return pluginSid;
    }

    /**
     * Setter method for pluginSid.
     *
     * @param aPluginSid the new value for pluginSid
     */
    public void setPluginSid(int aPluginSid) {
        pluginSid = aPluginSid;
    }

    /**
     * Access method for timestamp.
     *
     * @return the current value of timestamp
     */
    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    /**
     * Setter method for timestamp.
     *
     * @param aTimestamp the new value for timestamp
     */
    public void setTimestamp(LocalDateTime aTimestamp) {
        timestamp = aTimestamp;
    }

    /**
     * Access method for srcHost.
     *
     * @return the current value of srcHost
     */
    public byte[] getSrcHost() {
        return srcHost;
    }

    /**
     * Setter method for srcHost.
     *
     * @param aSrcHost the new value for srcHost
     */
    public void setSrcHost(byte[] aSrcHost) {
        srcHost = aSrcHost;
    }

    /**
     * Access method for dstHost.
     *
     * @return the current value of dstHost
     */
    public byte[] getDstHost() {
        return dstHost;
    }

    /**
     * Setter method for dstHost.
     *
     * @param aDstHost the new value for dstHost
     */
    public void setDstHost(byte[] aDstHost) {
        dstHost = aDstHost;
    }

    /**
     * Access method for srcNet.
     *
     * @return the current value of srcNet
     */
    public byte[] getSrcNet() {
        return srcNet;
    }

    /**
     * Setter method for srcNet.
     *
     * @param aSrcNet the new value for srcNet
     */
    public void setSrcNet(byte[] aSrcNet) {
        srcNet = aSrcNet;
    }

    /**
     * Access method for dstNet.
     *
     * @return the current value of dstNet
     */
    public byte[] getDstNet() {
        return dstNet;
    }

    /**
     * Setter method for dstNet.
     *
     * @param aDstNet the new value for dstNet
     */
    public void setDstNet(byte[] aDstNet) {
        dstNet = aDstNet;
    }

    /**
     * Access method for cnt.
     *
     * @return the current value of cnt
     */
    public int getCnt() {
        return cnt;
    }

    /**
     * Setter method for cnt.
     *
     * @param aCnt the new value for cnt
     */
    public void setCnt(int aCnt) {
        cnt = aCnt;
    }

    /**
     * Compares the key for this instance with another AcAcidEvent.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class AcAcidEvent and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof AcAcidEvent)) {
            return false;
        }
        AcAcidEvent that = (AcAcidEvent) other;
        Object myCtx = this.getCtx();
        Object yourCtx = that.getCtx();
        if (myCtx==null ? yourCtx!=null : !myCtx.equals(yourCtx)) {
            return false;
        }
        if (this.getDeviceId() != that.getDeviceId()) {
            return false;
        }
        if (this.getPluginId() != that.getPluginId()) {
            return false;
        }
        if (this.getPluginSid() != that.getPluginSid()) {
            return false;
        }
        Object myTimestamp = this.getTimestamp();
        Object yourTimestamp = that.getTimestamp();
        if (myTimestamp==null ? yourTimestamp!=null : !myTimestamp.equals(yourTimestamp)) {
            return false;
        }
        Object mySrcHost = this.getSrcHost();
        Object yourSrcHost = that.getSrcHost();
        if (mySrcHost==null ? yourSrcHost!=null : !mySrcHost.equals(yourSrcHost)) {
            return false;
        }
        Object myDstHost = this.getDstHost();
        Object yourDstHost = that.getDstHost();
        if (myDstHost==null ? yourDstHost!=null : !myDstHost.equals(yourDstHost)) {
            return false;
        }
        Object mySrcNet = this.getSrcNet();
        Object yourSrcNet = that.getSrcNet();
        if (mySrcNet==null ? yourSrcNet!=null : !mySrcNet.equals(yourSrcNet)) {
            return false;
        }
        Object myDstNet = this.getDstNet();
        Object yourDstNet = that.getDstNet();
        if (myDstNet==null ? yourDstNet!=null : !myDstNet.equals(yourDstNet)) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another AcAcidEvent.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof AcAcidEvent)) return false;
        return this.equalKeys(other) && ((AcAcidEvent)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        if (getCtx() == null) {
            i = 0;
        } else {
            i = getCtx().hashCode();
        }
        result = 37*result + i;
        i = getDeviceId();
        result = 37*result + i;
        i = getPluginId();
        result = 37*result + i;
        i = getPluginSid();
        result = 37*result + i;
        if (getTimestamp() == null) {
            i = 0;
        } else {
            i = getTimestamp().hashCode();
        }
        result = 37*result + i;
        if (getSrcHost() == null) {
            i = 0;
        } else {
            i = getSrcHost().hashCode();
        }
        result = 37*result + i;
        if (getDstHost() == null) {
            i = 0;
        } else {
            i = getDstHost().hashCode();
        }
        result = 37*result + i;
        if (getSrcNet() == null) {
            i = 0;
        } else {
            i = getSrcNet().hashCode();
        }
        result = 37*result + i;
        if (getDstNet() == null) {
            i = 0;
        } else {
            i = getDstNet().hashCode();
        }
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[AcAcidEvent |");
        sb.append(" ctx=").append(getCtx());
        sb.append(" deviceId=").append(getDeviceId());
        sb.append(" pluginId=").append(getPluginId());
        sb.append(" pluginSid=").append(getPluginSid());
        sb.append(" timestamp=").append(getTimestamp());
        sb.append(" srcHost=").append(getSrcHost());
        sb.append(" dstHost=").append(getDstHost());
        sb.append(" srcNet=").append(getSrcNet());
        sb.append(" dstNet=").append(getDstNet());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("ctx", getCtx());
        ret.put("deviceId", Integer.valueOf(getDeviceId()));
        ret.put("pluginId", Integer.valueOf(getPluginId()));
        ret.put("pluginSid", Integer.valueOf(getPluginSid()));
        ret.put("timestamp", getTimestamp());
        ret.put("srcHost", getSrcHost());
        ret.put("dstHost", getDstHost());
        ret.put("srcNet", getSrcNet());
        ret.put("dstNet", getDstNet());
        return ret;
    }

}
