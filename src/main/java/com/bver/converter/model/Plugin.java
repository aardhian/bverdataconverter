// Generated with g9.

package com.bver.converter.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name="plugin", schema = "alienvault")
@IdClass(Plugin.PluginId.class)
public class Plugin implements Serializable {

	/**
     * IdClass for primary key when using JPA annotations
     */
    public class PluginId implements Serializable {
        int id;
        byte[] ctx;
    }

    /** Primary key. */
    protected static final String PK = "PluginPrimary";

    /**
     * The optimistic lock. Available via standard bean get/set operations.
     */
    @Version
    @Column(name="LOCK_FLAG")
    private Integer lockFlag;

    /**
     * Access method for the lockFlag property.
     *
     * @return the current value of the lockFlag property
     */
    public Integer getLockFlag() {
        return lockFlag;
    }

    /**
     * Sets the value of the lockFlag property.
     *
     * @param aLockFlag the new value of the lockFlag property
     */
    public void setLockFlag(Integer aLockFlag) {
        lockFlag = aLockFlag;
    }

    @Id
    @Column(nullable=false, length=16)
    private byte[] ctx;
    @Id
    @Column(nullable=false, precision=10)
    private int id;
    @Column(nullable=false, precision=5)
    private short type;
    @Column(nullable=false, length=100)
    private String name;
    private String description;
    @Column(name="product_type", nullable=false, precision=10)
    private int productType;
    private String vendor;

    /** Default constructor. */
    public Plugin() {
        
    }

    /**
     * Access method for ctx.
     *
     * @return the current value of ctx
     */
    public byte[] getCtx() {
        return ctx;
    }

    /**
     * Setter method for ctx.
     *
     * @param aCtx the new value for ctx
     */
    public void setCtx(byte[] aCtx) {
        ctx = aCtx;
    }

    /**
     * Access method for id.
     *
     * @return the current value of id
     */
    public int getId() {
        return id;
    }

    /**
     * Setter method for id.
     *
     * @param aId the new value for id
     */
    public void setId(int aId) {
        id = aId;
    }

    /**
     * Access method for type.
     *
     * @return the current value of type
     */
    public short getType() {
        return type;
    }

    /**
     * Setter method for type.
     *
     * @param aType the new value for type
     */
    public void setType(short aType) {
        type = aType;
    }

    /**
     * Access method for name.
     *
     * @return the current value of name
     */
    public String getName() {
        return name;
    }

    /**
     * Setter method for name.
     *
     * @param aName the new value for name
     */
    public void setName(String aName) {
        name = aName;
    }

    /**
     * Access method for description.
     *
     * @return the current value of description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Setter method for description.
     *
     * @param aDescription the new value for description
     */
    public void setDescription(String aDescription) {
        description = aDescription;
    }

    /**
     * Access method for productType.
     *
     * @return the current value of productType
     */
    public int getProductType() {
        return productType;
    }

    /**
     * Setter method for productType.
     *
     * @param aProductType the new value for productType
     */
    public void setProductType(int aProductType) {
        productType = aProductType;
    }

    /**
     * Access method for vendor.
     *
     * @return the current value of vendor
     */
    public String getVendor() {
        return vendor;
    }

    /**
     * Setter method for vendor.
     *
     * @param aVendor the new value for vendor
     */
    public void setVendor(String aVendor) {
        vendor = aVendor;
    }

    /**
     * Compares the key for this instance with another Plugin.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Plugin and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Plugin)) {
            return false;
        }
        Plugin that = (Plugin) other;
        if (this.getId() != that.getId()) {
            return false;
        }
        Object myCtx = this.getCtx();
        Object yourCtx = that.getCtx();
        if (myCtx==null ? yourCtx!=null : !myCtx.equals(yourCtx)) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Plugin.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Plugin)) return false;
        return this.equalKeys(other) && ((Plugin)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getId();
        result = 37*result + i;
        if (getCtx() == null) {
            i = 0;
        } else {
            i = getCtx().hashCode();
        }
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Plugin |");
        sb.append(" id=").append(getId());
        sb.append(" ctx=").append(getCtx());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("id", Integer.valueOf(getId()));
        ret.put("ctx", getCtx());
        return ret;
    }

}
