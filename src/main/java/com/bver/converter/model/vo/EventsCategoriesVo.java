package com.bver.converter.model.vo;

import java.util.Date;

public class EventsCategoriesVo {
	private long id;
    private Date createdDate;
    private String categoryId;
    private String name;
    private long eventCnt;
    
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public String getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public long getEventCnt() {
		return eventCnt;
	}
	public void setEventCnt(long eventCnt) {
		this.eventCnt = eventCnt;
	}
    
    
}
