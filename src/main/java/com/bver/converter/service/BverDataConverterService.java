package com.bver.converter.service;

public interface BverDataConverterService {
	
	void executeEventsBySensorDs();
	void executeEventsCategories();
	void executeEventsSecTrenLastDay();
	void executeEventsSecTrenLastWeek();
	void executeEventsSiemVsLogger();
	
}
