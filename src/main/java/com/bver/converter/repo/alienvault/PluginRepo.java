package com.bver.converter.repo.alienvault;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bver.converter.model.Plugin;
import com.bver.converter.model.Plugin.PluginId;

@Repository
public interface PluginRepo extends JpaRepository<Plugin, PluginId> {

	@Override
    public List<Plugin> findAll();
	
	
}
